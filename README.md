# soal-shift-sisop-modul-4-D07-2022

Praktikum 3 Sistem Operasi 2022

## Anggota Kelompok
- Fitra Agung Diassyah Putra - 5025201072
- Ananda Hadi Saputra - 5025201148
- Fajar Zuhri Hadiyanto - 5025201248

## Video Demonstrasi
Berikut ini merupakan video demonstrasi agar laporan di README.md jadi makin singkat

[![Video Demonstrasi](http://img.youtube.com/vi/Og2ZkUyP1no/0.jpg)](http://www.youtube.com/watch?v=Og2ZkUyP1no)

## Soal 1
### bagian a, b, c
Berikut ini merupakan isi dari folder Animeku_test yang telah terenkripsi

![1a](/uploads/5def97fe968feef850f1f79df1786046/1a.png)

Berikut ini merupakan isi dari folder Animeku_test setelah direname menjadi test, terlihat bahwa nama asli filenya yaitu test_file.txt

![1c](/uploads/338869eed98fba3cfb42db7d5adfe8cc/1c.png)

Setelah folder test direname kembali menjadi Animeku_test, maka isi folder akan kembali terenkripsi seperti pada gambar di awal.

### bagian d

Berikut ini merupakan isi dari file Wibu.log. Terlihat bahwa terdapat beberapa kali proses enkripsi dan decode akibat proses rename.

![1d](/uploads/3e9bf13987d818d340cc690461762d21/1d.png)

### bagian e

Berikut ini merupakan isi dari folder Animeku_test2 yang telah terenkripsi

![1e1](/uploads/89593499a7094c292a1730214659dc0c/1e1.png)

Berikut ini merupakan isi dari folder Animeku_test2 setelah direname menjadi test2, terlihat bahwa nama asli folder di dalamnya yaitu test3 dan nama file aslinya yaitu test_file.txt

![1e2](/uploads/f72f05fb45400027e5a36cdfdd61a6b4/1e2.png)

## Soal 2
### bagian a, b, c

Beriku ini merupakan isi dari folder IAN_test yang telah terenkripsi

![2a](/uploads/ccc1dce2dcb9d5e77f8a9e2458f62cfe/2a.png)

Berikut ini merupakan isi dari folder IAN_test setelah direname menjadi test. Terlihat bahwa nama asli filenya yaitu test_file.txt

![2c](/uploads/44bc89867d4c24bc163c6e5ca4017bd1/2c.png)

Setelah folder test direname kembali menjadi IAN_test.txt, maka isi folder akan kembali terenkripsi

### bagian d, e

Berikut ini merupakan isi dari file hayolongapain_D07.log, setelah terdapat beberapa operasi seperti buat dan hapus folder serta file.

![2d](/uploads/d8b2788534f38da7d7dfaf7be595a04e/2d.png)

## Soal 3

Berikut ini merupakan isi dari folder nam_do-saq_test yang telah dienkripsi

![3a](/uploads/6d69a20628238e21a6cdcf244c802fa2/3a.png)

Berikut ini merupakan isi dari folder nam_do-saq_test setelah direname menjadi test. Terlihat bahwa nama folder aslinya yaitu Animeku_test dan IAN_test, dengan masing2 file didalamnya yang telah dienkripsi sesuai dengan ketentuan nomor 1 dan 2

![3c](/uploads/80b94411a335c65dbe7fb1a327313ecb/3c.png)

Berikut ini merupakan isi dari folder test/Animeku_test setelah diganti menjadi test/test-animeku dan folder test/IAN_test setelah diganti menjadi test/test-ian. Terlihat bahwa nama asli file di dalam test/test-animeku dan test/test-ian yaitu test_file.txt.

![3c2](/uploads/0a5d647edbe04c5959f7e22654eadecf/3c2.png)

## !!! KESULITAN !!!
Materinya sulit dipahami, sekelompok ga ada yang ngerti teori FUSE, apalagi praktikumnya. Butuh didemoin temen yang lain, baru bisa ngerti sebagian sih, but i think thats more than enough to make this report.

